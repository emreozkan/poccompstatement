sap.ui.define([
	'com/poit/compstatement/controller/BaseController'
], function(Controller) {
	"use strict";
	var aPayComponents = [];
	var aPayGroups = [];
	return Controller.extend("com.poit.compstatement.controller.Statement", {
		onInit: function() {
			this.getView().setModel(new sap.ui.model.json.JSONModel({
				PayComponents: [],
				PayGroups: []
			}), "main");

			var oRouter = this.getRouter();
			oRouter.getRoute("statement").attachMatched(this._onRouteMatched, this);
			//aPayComponents = this.getView().getModel().getData().myPayComponents;
		},
		onPanelExpand: function(oEvent) {
			var groupId = oEvent.getSource().getBindingContext("main").getProperty("groupId");
			var groupName = oEvent.getSource().getBindingContext("main").getProperty("groupName");
			var StatementId = this.getView().getBindingContext("statements").getProperty("StatementId");
			var Year = this.getView().getBindingContext("statements").getProperty("StatementYear");

			var entityName = StatementId + groupName;
			var oTable = new sap.m.Table({
				inset: false,
				enableBusyIndicator: true,
				showSeparators: "None",
				mode: sap.m.ListMode.None,
				columns: [
					new sap.m.Column({
						width: "5em",
						header: new sap.m.Label({
							text: "Component Name"
						})
					}),
					new sap.m.Column({
						width: "2em",
						header: new sap.m.Label({
							text: "Pay Month"
						})
					}),
					new sap.m.Column({
						width: "2em",
						header: new sap.m.Label({
							text: "Monthly Amount"
						})
					}),
					new sap.m.Column({
						width: "3em",
						hAlign: "Right",
						header: new sap.m.Label({
							text: "LTI(3) Units"
						})
					}),
					new sap.m.Column({
						width: "3em",
						hAlign: "Right",
						header: new sap.m.Label({
							text: "Earned Value"
						})
					}),
					new sap.m.Column({
						width: "3em",
						hAlign: "Right",
						header: new sap.m.Label({
							text: "Cash Value"
						})
					}),
					new sap.m.Column({
						width: "6em",
						header: new sap.m.Label({
							text: "Remarks"
						})
					})
				]
			});

			//var myPayComponents = this.getView().getModel().getData().myPayComponents;
			// Do filtering

			//this.getView().getModel().setData({"myEntity":dataToFilter});
			var oNewPayComponents = [];
			//var dataToFilter = [];
			for (var i = 0; i < aPayComponents.length; i++) {
				var lPayComponents = aPayComponents[i];
				if (lPayComponents.groupId.toString() === groupId && lPayComponents.StatementId.toString() === StatementId) {
					oNewPayComponents.push(lPayComponents);
					//oTable.addItem(lPayComponents)
				}
			}

			//this.getView().getModel().setData({"myPayComponents":oNewPayComponents});
			var json = {};
			json[entityName] = oNewPayComponents;
			var oNewModel = new sap.ui.model.json.JSONModel(json);
			//this.getView().setModel(oNewModel);
			oEvent.getSource().setModel(oNewModel);
			oTable.bindItems({
				path: "/" + entityName, //statements>/PayComponents",
				template: new sap.m.ColumnListItem({
					cells: [new sap.m.Text({
							text: "{componentName} {additionalInfo}"
						}),
						new sap.m.Text({
							text: "{payMonth}"
						}),

						new sap.m.Text({
							text: {
								path: "monthlyAmount",
								type: new sap.ui.model.type.Float({
									minFractionDigits: 2,
									maxFractionDigits: 2
								})
							}
						}),
						new sap.m.Text({
							text: {
								path: "LtiUnits",
								type: new sap.ui.model.type.Float({
									minFractionDigits: 2,
									maxFractionDigits: 2
								})
							}
						}),
						new sap.m.Text({
							text: {
								path: "yearEarned",
								type: new sap.ui.model.type.Float({
									minFractionDigits: 2,
									maxFractionDigits: 2
								})
							}
						}),
						new sap.m.Text({
							text: {
								path: "yearCash",
								type: new sap.ui.model.type.Float({
									minFractionDigits: 2,
									maxFractionDigits: 2
								})
							}
						}),
						new sap.m.Text({
							text: "{remarks}"
						})
					]
				})
			});
			oEvent.getSource().removeAllContent();
			oEvent.getSource().addContent(oTable);
		},
		onNavBack: function() {
			this.getRouter().navTo("statements");
		},
		_onRouteMatched: function(oEvent) {
			var oModel = this.getView().getModel("statements"),
				oMainModel = this.getView().getModel("main");

			var oArgs, oView;
			oArgs = oEvent.getParameter("arguments");
			oView = this.getView();

			oView.bindElement({
				path: "statements>/Statements('" + oArgs.statementId + "')",
				events: {
					change: this._onBindingChange.bind(this),
					dataRequested: function(oEvent) {
						oView.setBusy(true);
					},
					dataReceived: function(oEvent) {

						var oPayData = this.getView().getBindingContext("statements").getObject();
						oPayData.Lti3T10 = 320;
						oPayData.Sti1 = 330;
						oPayData.BaseSalary = 6220;
						oPayData.Lti3OutT10 = 310;
						if (oPayData.EmployeeName==="" || oPayData.EmployeeName===undefined){oPayData.EmployeeName = "Emre Ozkan";}
						if (oPayData.Department===""|| oPayData.Department===undefined){oPayData.Department="SAP HR Development";}
						if (oPayData.Title==="" || oPayData.Title===undefined){oPayData.Title="Senior Developer";}
						if (oPayData.Country==="" || oPayData.Country===undefined){oPayData.Country="Singapore";}
						if (oPayData.JoinDate==="" || oPayData.JoinDate===undefined){oPayData.JoinDate="13 Jan 2011";}
						if (oPayData.Email==="" || oPayData.Email===undefined){oPayData.Email="emre.ozkan@presenceofit.com.au";}
						if (oPayData.Phone==="" || oPayData.Phone===undefined){oPayData.Phone="+61 423 598174";}
						oModel.read("/PayComponents", {
							// async: false, not used in V2					
							success: function(oData) {
								aPayComponents = oData.results;
								// Combine aPayData with oData.results
								// Update recieved fields into template data 
								for (var j = 0; j < aPayComponents.length; j++) {
									var lPayComponents = aPayComponents[j];
									if (lPayComponents.StatementId === oArgs.statementId) {
										switch (lPayComponents.componentName) {
											case 'Base Salary':
												lPayComponents.monthlyAmount = oPayData.BaseSalary;
												break;
											case 'T10':
												lPayComponents.monthlyAmount = oPayData.Lti3T10;
												break;
											case 'STI(1)':
												lPayComponents.monthlyAmount = oPayData.Sti1;
												break;
												// case 'STI(1)':	
										}
									}
									aPayComponents[j] = lPayComponents;
								}
								oMainModel.setProperty("/PayComponents", aPayComponents);
								console.log("Pay Components read successfull");
							},
							error: function(oEvent) {
								console.log("Pay Components read failed!");
							}
						});

						oModel.read("/PayGroups", {
							success: function(oData) {
								aPayGroups = oData.results;
								// calculate totals for groups
								for (var i = 0; i < aPayGroups.length; i++) {
									var lPayGroup = aPayGroups[i];
									lPayGroup.monthlyAmount = 0;
									lPayGroup.yearEarned = 0;
									lPayGroup.yearCash = 0;
									for (var j = 0; j < aPayComponents.length; j++) {
										var lPayComponents = aPayComponents[j];
										if (lPayComponents.groupId === lPayGroup.groupId && lPayComponents.StatementId === oArgs.statementId) {
											if (!isNaN(parseFloat(lPayComponents.monthlyAmount))) {
												lPayGroup.monthlyAmount = lPayGroup.monthlyAmount + parseFloat(lPayComponents.monthlyAmount);
											}
											if (!isNaN(parseFloat(lPayComponents.yearEarned))) {
												lPayGroup.yearEarned = lPayGroup.yearEarned + parseFloat(lPayComponents.yearEarned);
											}
											if (!isNaN(parseFloat(lPayComponents.yearCash))) {
												lPayGroup.yearCash = lPayGroup.yearCash = parseFloat(lPayGroup.yearCash);
											}
										}

										aPayGroups[i] = lPayGroup;
									}

									oMainModel.setProperty("/PayGroups", aPayGroups);
									console.log("Pay Groups read successfull");
								}
							},
							error: function(oEvent) {
								console.log("Pay Groups read failed!");
							}

						});

						oView.setBusy(false);
					}.bind(this)
				}
			});
		},
		_onBindingChange: function(oEvent) {
			var oElementBinding = this.getView().getElementBinding();
			// No data for the binding
			if (oElementBinding && !oElementBinding.getBoundContext()) {
				this.getRouter().getTargets().display("home");
			}
		},
		onTest: function(oValue) {
			return oValue == "1";
		},
		toFloat: function(oValue) {
			// var oFormat = new sap.ui.core.format;
			// oFormat.value = oValue; 
			// return oFormat.NumberFormat({type: formatOptions:{minFractionDigits: 2}})
			// var oModel = new sap.ui.model;
			// oModel.
			var aFloat = new sap.ui.model.type.Float({
				formatOptions: {
					minFractionDigits: 2
				}
			});
			return aFloat.formatValue(oValue, "float");
		},

		getGroupHeader: function(oGroup) {
			return new sap.m.GroupHeaderListItem({
				title: oGroup.key,
				type: "Active",
				upperCase: false
			});
		}
	});

});