 sap.ui.define([
	"com/poit/compstatement/controller/BaseController"
], function(Controller) {
	"use strict";

	return Controller.extend("com.poit.compstatement.controller.Configure", {
		onChangeYear: function(oEvent) {
			var oTable = oEvent.getSource().getParent().getParent(),
				sYear  = oEvent.getParameter("selectedItem").getKey();
			
			oTable.getBinding("items").filter(new sap.ui.model.Filter({path:'StatementYear',operator:'EQ',value1:sYear}));
		}
	});

});