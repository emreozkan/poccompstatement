sap.ui.define([
	"com/poit/compstatement/controller/BaseController",
	"sap/m/ActionSheet",
	"sap/m/Button"
], function(Controller,ActionSheet,Button) {
	"use strict";

	return Controller.extend("com.poit.compstatement.controller.App", {
		onHandleLogoffPress: function(oEvent) {
			document.location="/logout.html";
		},
		onUserItemPressed: function(oEvent) {
			/*
			var oActionSheet = new ActionSheet({
				placement:"Bottom",
				buttons:[new Button({
					icon:"sap-icon://log",
					text:"Logoff",
					press:this.onHandleLogoffPress
				})]});
			oActionSheet.openBy(oEvent.getSource());
			*/
		}
	});

});