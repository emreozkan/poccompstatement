sap.ui.define([
	"com/poit/compstatement/controller/BaseController"
], function(Controller) {
	"use strict";

	return Controller.extend("com.poit.compstatement.controller.Statements", {
		onNavBack:function(){
			this.getRouter().navTo("appHome");
		},
		onDisplayStatement: function(oEvent){ 
			this.getRouter().navTo("statement",{statementId:oEvent.getSource().getBindingContext("statements").getProperty("StatementId")});
		}
	});

});