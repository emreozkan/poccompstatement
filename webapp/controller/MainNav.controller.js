sap.ui.define([
	"com/poit/compstatement/controller/BaseController"
], function(Controller) {
	"use strict";

	return Controller.extend("com.poit.compstatement.controller.MainNav", {
		onTilePress: function(oEvent) {
			this.getRouter().navTo(oEvent.getSource().data("route"));
		}
	});

});