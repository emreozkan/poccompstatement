jQuery.sap.declare("com.poit.compstatement.controller.blocks.ProjectedBlock");
jQuery.sap.require("sap.uxap.BlockBase");

sap.uxap.BlockBase.extend("com.poit.compstatement.controller.blocks.ProjectedBlock", {
    metadata: {
        views: {
            Collapsed: {
                viewName: "com.poit.compstatement.controller.blocks.ProjectedBlock",
                type: "XML"
            },
            Expanded: {
                viewName: "com.poit.compstatement.controller.blocks.ProjectedBlock",
                type: "XML"
            }
        }
    }
});