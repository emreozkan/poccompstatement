sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"com/poit/compstatement/model/models",
	"sap/ui/core/util/MockServer",
	"sap/ui/model/odata/v2/ODataModel"
], function(UIComponent, Device, models, MockServer, ODataModel) {
	"use strict";

	return UIComponent.extend("com.poit.compstatement.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function() {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// set the device model
			this.setModel(models.createDeviceModel(), "device");
			
			this.setModel(models.createUiModel(),"UiControl");
			
			// Create mockserver
            var oMockServer = new MockServer({
                rootUri: "https://mockserver/"
            });			
            oMockServer.simulate("webapp/model/metadata.xml", "webapp/model/");
            oMockServer.start();
            
            this.setModel(new ODataModel("https://mockserver",true),"statements");
			
			this.getModel("xrates").attachRequestCompleted(function(oEvent){
				var oModel = oEvent.getSource(),
					oRates = oModel.getData().rates,
					aRates = [];
					
				aRates = Object.keys(oRates).map(function(currency){
					return {"currency":currency,"rate":oRates[currency]};
				});
				
				aRates.unshift({"currency":"SGD","rate":"1.000"});
				
				oModel.setProperty("/ratesTable",aRates);
				
			});
			
			if(!this.getModel("user").getProperty("/name")){
				this.getModel("user").attachEventOnce("requestCompleted",function(){
					
				},this);
			}
			// Initialize router
			this.getRouter().initialize();
		},

		/**
		 * This method can be called to determine whether the sapUiSizeCompact or sapUiSizeCozy
		 * design mode class should be set, which influences the size appearance of some controls.
		 * @public
		 * @return {string} css class, either 'sapUiSizeCompact' or 'sapUiSizeCozy' - or an empty string if no css class should be set
		 */
		getContentDensityClass: function() {
			if (this._sContentDensityClass === undefined) {
				// check whether FLP has already set the content density class; do nothing in this case
				if (jQuery(document.body).hasClass("sapUiSizeCozy") || jQuery(document.body).hasClass("sapUiSizeCompact")) {
					this._sContentDensityClass = "";
				} else if (!Device.support.touch) { // apply "compact" mode if touch is not supported
					this._sContentDensityClass = "sapUiSizeCompact";
				} else {
					// "cozy" in case of touch support; default for most sap.m controls, but needed for desktop-first controls like sap.ui.table.Table
					this._sContentDensityClass = "sapUiSizeCozy";
				}
			}
			return this._sContentDensityClass;
		}

	});

});